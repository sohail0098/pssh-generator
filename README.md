## PSSH-Generator
This script generates the correct pssh for dash, ss and hls.

## How to use
The input needs to be named as "video.mp4" and run the following command:

* python pssh.py (takes video.mp4 as input)
* python psshss.py (url) (takes a url input [link to .ism])